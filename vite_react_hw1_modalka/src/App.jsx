import { useState } from 'react';
import Button from './components/Button/Button.jsx'; // якщо в export не default, то тут в лапках "Button"
import ModalFirst from './components/ModalFirst/ModalFirst.jsx';
import ModalSecond from './components/ModalSecond/ModalSecond.jsx';

function App() {
  const [isFirstModal, setIsFirstModal] = useState(false);
  const handleFirstModal = () => {
    setIsFirstModal((prevState) => !prevState);
  }

  const [isSecondModal, setIsSecondModal] = useState(false);
  const handleSecondModal = () => {
    setIsSecondModal((prevState) => !prevState);
  }

  return (
    <>
      <header className="header">
        <Button className="btn__first-modal" onClick={handleFirstModal}>Open first modal</Button>
        <Button className="btn__second-modal" onClick={handleSecondModal}>Open second modal</Button>
      </header>
      {isFirstModal && <ModalFirst close={handleFirstModal} />}
      {isSecondModal && <ModalSecond close={handleSecondModal} />}
    </>
  )
};

export default App;


// import React, { useState } from 'react' //в цьому варіанті ми витягуємо спочатку дефолтну всю бібліотеку React (тут при бажанні її можна переназвати і надалі використовувати нову назву)
// Тоді в середині return замість пустих <> пишемо <React.Fragment> //в цьому варіанті ми в першому рядку витягнули всю бібліотеку React, тому тут витягуємо функцію чи властивість з бібліотеки