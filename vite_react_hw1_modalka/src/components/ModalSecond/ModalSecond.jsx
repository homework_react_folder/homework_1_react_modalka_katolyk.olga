import React from "react";
import "../ModalBase/ModalBase.scss";
import "./ModalSecond.scss";
import ModalWrapper from "../ModalBase/ModalWrapper.jsx";
import ModalBox from "../ModalBase/ModalBox.jsx";
import ModalClose from "../ModalBase/ModalClose.jsx";
import ModalHeader from "../ModalBase/ModalHeader.jsx"
import ModalBody from "../ModalBase/ModalBody.jsx";
import ModalFooter from "../ModalBase/ModalFooter.jsx";
// import Button from "../Button/Button.jsx";

const ModalSecond = ({close}) => {
    return (
        <ModalWrapper onClick={close}>
            <ModalBox className="modal-second">
                <ModalClose onClick={close} />
                <img src="" className="modal-header__img"/>
                <ModalHeader>                    
                    Product Delete!
                </ModalHeader>
                <ModalBody>By clicking the “Yes, Delete” button, PRODUCT NAME will be deleted.</ModalBody>
                {/* <ModalFooter>
                    <Button className="button--violet" onClick={close}>NO, CANCEL</Button>
                    <Button className="button--white" onClick={close}>YES, DELETE</Button>
                </ModalFooter> */}
                <ModalFooter firstText="NO, CANCEL" secondaryText="YES, DELETE" firstClick={close} secondaryClick={close} />
            </ModalBox>
        </ModalWrapper>
    )
};

export default ModalSecond;