import React from "react";
import "../ModalBase/ModalBase.scss";
import "./ModalFirst.scss";
import ModalWrapper from "../ModalBase/ModalWrapper.jsx";
import ModalBox from "../ModalBase/ModalBox.jsx";
import ModalClose from "../ModalBase/ModalClose.jsx";
import ModalHeader from "../ModalBase/ModalHeader.jsx"
import ModalBody from "../ModalBase/ModalBody.jsx";
import ModalFooter from "../ModalBase/ModalFooter.jsx";
// import Button from "../Button/Button.jsx";

const ModalFirst = ({close}) => {
    return (
        <ModalWrapper onClick={close}>
            <ModalBox className="modal-first">
                <ModalClose onClick={close}/>
                <ModalHeader>Add Product “NAME”</ModalHeader>
                <ModalBody>Description for you product</ModalBody>
                {/* <ModalFooter >
                    <Button className="button--violet" onClick={close}>ADD TO FAVORITE</Button>
                </ModalFooter> */}
                <ModalFooter firstText="ADD TO FAVORITE" firstClick={close} />
            </ModalBox>
        </ModalWrapper>
    )
};

export default ModalFirst;