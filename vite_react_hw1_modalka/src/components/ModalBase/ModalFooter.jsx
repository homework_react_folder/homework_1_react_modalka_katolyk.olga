import React from 'react';
import Button from '../Button/Button';

const ModalFooter = ({ children, firstText, secondaryText, firstClick, secondaryClick }) => {
    // return (
    //     <div className="modal-footer">
    //         {children}
    //     </div>
    // );

    return (
        <div className="modal-footer">
            
            {children}

            {firstText && (<Button className="button--violet" onClick={firstClick}>
                {firstText}
            </Button>)}

            {secondaryText && (<Button className="button--white" onClick={secondaryClick}>
                {secondaryText}
            </Button>)}
            
        </div>
    )
};

export default ModalFooter;