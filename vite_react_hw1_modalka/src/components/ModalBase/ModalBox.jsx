import React from "react";
import cn from "classnames";

const ModalBox = ({children, className}) => {
    return (        
        <div className={cn("modal-box", className)}>
            {children}
        </div>   
    )
};

export default ModalBox;