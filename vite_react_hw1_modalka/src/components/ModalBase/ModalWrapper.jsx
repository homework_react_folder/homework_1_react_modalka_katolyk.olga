import React from 'react';

const ModalWrapper = ({children, onClick}) => {
    return (
        <div className="modal-wrapper" onClick={(e) => {
            if (e.target === e.currentTarget) {
                onClick();
            }}}>
            {children}
        </div>
    )
};

export default ModalWrapper;