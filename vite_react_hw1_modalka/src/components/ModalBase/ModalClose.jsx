import React from 'react';
import CloseX from './icons/close_X.svg?react'

const ModalClose = ({onClick}) => {
    return (
        <button type="button" className="modal-close" onClick={onClick}>
            <CloseX />
        </button>
    )
};

export default ModalClose;